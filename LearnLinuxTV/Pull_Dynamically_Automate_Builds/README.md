# Using Ansible "Pull" Mode to Dynamically Automate Server/Workstation Builds

## YouTube Playlist
* https://www.youtube.com/watch?v=sn1HQq_GFNE

## wiki.learnlinux.tv
* https://wiki.learnlinux.tv/index.php/Episode_Notes
  * https://wiki.learnlinux.tv/index.php/Using_Ansible_Pull

## github.com
* https://github.com/jlacroix82/ansible_pull_tutorial.git
* https://github.com/LearnLinuxTV/ansible_pull_tutorial

```
01:14 - What is "ansible-pull"?
07:00 - Setting up the Git Repository
14:13 - Running playbooks via ansible-pull
```



```
sudo ansible-pull -U https://github.com/LearnLinuxTV/ansible_pull_tutorial
```
