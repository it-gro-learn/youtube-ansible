# Using Ansible to automate your Laptop and Desktop configs!

## YouTube Playlist
* https://www.youtube.com/watch?v=gIDywsGBqf4

```
Individual sections:
2:29: Notes about my config
17:22: Setting up a Git repo
29:52: Creating the Playbook
```

## wiki.learnlinux.tv
* https://wiki.learnlinux.tv/index.php/Episode_Notes
  * https://wiki.learnlinux.tv/index.php/Using_Ansible_to_configure_your_Desktops_and_Laptops

## github.com
* https://github.com/LearnLinuxTV/ansible_desktop_tutorial
* https://github.com/LearnLinuxTV/personal_ansible_desktop_configs

