## LearnLinuxTV
* https://www.youtube.com/user/JtheLinuxguy
* http://www.learnlinux.tv
* https://github.com/LearnLinuxTV
* https://twitter.com/jaythelinuxguy
* https://jaylacroix.com/

### Ansible
* https://www.youtube.com/c/LearnLinuxtv/search?query=ansible
* 1) Getting started with Ansible https://www.youtube.com/playlist?list=PLT98CRl2KxKEUHie1m24-wkyHpEsa4Y70
* 2) Encrypting Files with Ansible Vault https://www.youtube.com/watch?v=xeBnAbmt3Wk
* 3) Using Ansible "Pull" Mode to Dynamically Automate Server/Workstation Builds https://www.youtube.com/watch?v=sn1HQq_GFNE
* 4) Using Ansible to automate your Laptop and Desktop configs! https://www.youtube.com/watch?v=gIDywsGBqf4
