# Encrypting Files with Ansible Vault


## YouTube Playlist
* https://www.youtube.com/watch?v=xeBnAbmt3Wk

```
Individual sections:
00:43 - Creating & Storing the vault password
04:42 - Encrypting and Decrypting Files
08:39 - Editing and viewing encrypted files directly
10:21 - Using vault-encrypted files within playbooks
18:31 - Changing the vault password
```

## wiki.learnlinux.tv
* https://wiki.learnlinux.tv/index.php/Episode_Notes
  * https://wiki.learnlinux.tv/index.php/Encrypting_Files_with_Ansible_Vault


## Password Generators
* KeyPassXC


## Content

```
touch     ~/.vault_key
chmod 600 ~/.vault_key
nano      ~/.vault_key
```


```
nano info.txt
cat info.txt
ansible-vault encrypt info.txt
cat info.txt
```

```
ansible-vault decrypt --vault-password-file ~/.vault_key info.txt
cat info.txt
ansible-vault encrypt --vault-password-file ~/.vault_key info.txt
cat info.txt
```


```
ansible-vault edit --vault-password-file ~/.vault_key info.txt
cat info.txt
```

```
ansible-vault view --vault-password-file ~/.vault_key info.txt
```

```
ansible-vault encrypt --vault-password-file ~/.vault_key sudoers_ansible
```


```
sudo ansible-pull --vault-password-file ~/.vault_key -U https://github.com/LearnLinuxTV/ansible_pull_tutorial
```


```
ansible-vault rekey sudoers_ansible --vault-password-file ~/.vault_key 
```
* New password
