## Improving your Playbook
* https://ansible-tips-and-tricks.readthedocs.io/en/latest/os-dependent-tasks/variables/
* https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html


`install_tools.yml`

```
---

- hosts: all
  become: true
  tasks:

  - name: install sudo
    package:
      update_cache: yes
      name:
        - sudo

  - name: install tools
    when: ansible_distribution in ["Debian", "Ubuntu"]
    apt:
      update_cache: yes
      name:
        - htop
      state: latest

  - name: install tools
    package:
      update_cache: yes
      name:
        - nano
        - vim
        - tmux
        - mc
```

```
ansible-playbook --ask-become-pass install_tools.yml
ansible-playbook install_tools.yml

```
