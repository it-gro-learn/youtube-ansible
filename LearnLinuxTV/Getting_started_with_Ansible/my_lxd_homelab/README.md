## My Homelab

### ssh

```
ssh-keygen -t ed25519 -C "ansible" -f ~/.ssh/id_ansible_ed25519
cat  ~/.ssh/id_ansible_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGyZDap8kpQnbCS5w1fA1DiAYz1FOCi2mpHBAptTS2tc ansible
```

### LXD

#### lxc launch
```
lxc launch ubuntu:20.04          ub20-tut-ansible-01
lxc launch ubuntu:18.04          ub18-tut-ansible-01
lxc launch images:debian/buster  de10-tut-ansible-01
lxc launch images:debian/stretch de09-tut-ansible-01
lxc launch images:centos/8       ce83-tut-ansible-01
lxc launch images:centos/7       ce79-tut-ansible-01
```

#### config

```
ubuntu_containers=""
ubuntu_containers="${ubuntu_containers} ub20-tut-ansible-01 "
ubuntu_containers="${ubuntu_containers} ub18-tut-ansible-01 "

debian_containers=""
debian_containers="${debian_containers} de10-tut-ansible-01 "
debian_containers="${debian_containers} de09-tut-ansible-01 "

centos_containers=""
centos_containers="${centos_containers} ce83-tut-ansible-01 "
centos_containers="${centos_containers} ce79-tut-ansible-01 "

containers=""
containers="${containers} ${ubuntu_containers}"
containers="${containers} ${debian_containers}"
containers="${containers} ${centos_containers}"
```

#### bootstrap without ansible

##### root authorized_keys
```
for lxd_instance in ${containers}; do
  lxc shell ${lxd_instance} << "EOLXC"
      ls -la 
      test -d .ssh/ || mkdir .ssh/
      chmod 700 .ssh
      test -f .ssh/authorized_keys || touch .ssh/authorized_keys
      chmod 600 .ssh/authorized_keys
      ls -la .ssh
EOLXC

  lxc file push ~/.ssh/id_ansible_ed25519.pub ${lxd_instance}/root/.ssh/
  lxc file push ~/.ssh/id_rsa_lxd.pub         ${lxd_instance}/root/.ssh/
   
  lxc shell ${lxd_instance} << "EOLXC"
      ls -la .ssh
      cat /root/.ssh/*.pub >> /root/.ssh/authorized_keys
      ls -la .ssh
EOLXC
done
```

##### Ansible requirements Ubuntu

```
ubuntu_containers=""
ubuntu_containers="${ubuntu_containers} de10-tut-ansible-01 "
ubuntu_containers="${ubuntu_containers} de09-tut-ansible-01 "

for lxd_instance in ${ubuntu_containers}; do
  lxc shell ${lxd_instance} << "EOLXC"
EOLXC
done
```

##### Ansible requirements Debian

```
debian_containers=""
debian_containers="${debian_containers} de10-tut-ansible-01 "
debian_containers="${debian_containers} de09-tut-ansible-01 "

for lxd_instance in ${debian_containers}; do
  lxc shell ${lxd_instance} << "EOLXC"
    export DEBIAN_FRONTEND=noninteractive
    apt-get update
    apt-get install --yes openssh-server
    apt-get install --yes python3
    # apt-get install --yes sudo
EOLXC
done
```

##### Ansible requirements CentOS

```
centos_containers=""
centos_containers="${centos_containers} ce83-tut-ansible-01 "
centos_containers="${centos_containers} ce79-tut-ansible-01 "

for lxd_instance in ${centos_containers}; do
  lxc shell ${lxd_instance} << "EOLXC"
    yum install -y openssh-server
    systemctl start sshd
    # yum install -y sudo
    # yum install -y dnf
EOLXC
done
```

#### Check ssh port

```
for lxd_instance in ${containers}; do
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo ${lxd_instance}
  nmap -Pn ${lxd_instance}
done

```

#### Remove from ~/.ssh/known_hosts

```
for lxd_instance in ${containers}; do
  ip=$(host ub20-tut-ansible-01 | cut -d" " -f4)
  echo ${lxd_instance} $ip
  ssh-keygen -R ${lxd_instance} 2>/dev/null
  ssh-keygen -R ${ip} 2>/dev/null
done
```

#### Add to ~/.ssh/known_hosts

```
for lxd_instance in ${containers}; do
  echo ${lxd_instance}
  ssh -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -i ~/.ssh/id_ansible_ed25519 root\@"${lxd_instance}" "hostnamectl"
done
```

#### Check ssh connect (root)
```
for lxd_instance in ${containers}; do
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo ${lxd_instance}
  ssh -o IdentitiesOnly=yes -i ~/.ssh/id_ansible_ed25519 root\@"${lxd_instance}" "cat /etc/os-release "
done

```

#### LXD boot.autostart

```
for lxd_instance in ${containers}; do
  lxc config set ${lxd_instance} boot.autostart false
done
```

#### LXD start instances

```
for lxd_instance in ${containers}; do
  lxc start ${lxd_instance}
done

```

#### LXD delete instances
```
for lxd_instance in ${containers}; do
  lxc stop ${lxd_instance}
  lxc list ${lxd_instance}
  lxc delete ${lxd_instance}
done

```

### Bootstrap via ansible

```
ansible-playbook --tags bootstrap bootstrap.yml 
ansible all -m ping
```
https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html
https://docs.ansible.com/ansible/latest/collections/ansible/builtin/group_module.html#ansible-collections-ansible-builtin-group-module
