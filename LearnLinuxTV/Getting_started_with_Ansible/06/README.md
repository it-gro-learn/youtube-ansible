## Writing our first Playbook

`install_tools.yml`
```
---

- hosts: ubuntu,debian
  become: true
  tasks:

  - name: install tools
    apt:
      name: nano,vim,tmux,mc,htop
```



```
ansible-playbook --ask-become-pass install_tools.yml
```

```
ansible-playbook --ask-become-pass remove_htop.yml
```

