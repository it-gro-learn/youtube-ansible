## Tags

```
ansible-playbook --list-tags site.yml
```

```
ansible-playbook --tags centos site.yml

ansible-playbook --tags "apache,db" site.yml
```

```
ansible-playbook --tags db --ask-become-pass site_with_tags.yml
ansible-playbook --tags centos --ask-become-pass site_with_tags.yml
ansible-playbook --tags apache --ask-become-pass site_with_tags.yml
```
