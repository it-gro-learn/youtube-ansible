## Host Variables and Handlers

```
mkdir host_vars
mkdir roles/web_servers/handlers
touch host_vars/ub20-tut-ansible-01.yml
touch host_vars/ce83-tut-ansible-01.yml
```


`host_vars/ub20-tut-ansible-01.yml`
```
apache_package_name: apache2
apache_service: apache2
php_package_name: libapache2-mod-php
```

`host_vars/ce83-tut-ansible-01.yml`
```
apache_package_name: httpd
apache_service: httpd
php_package_name: php
```

`roles/web_servers/tasks/main.yml`
```
...
   package:
     name:
       - "{{ apache_package_name}}"
       - "{{ php_package_name}}"
...
   service:
     name: "{{ apache_service }}"
...
```


```
ansible-playbook site.roles.yml
```


`roles/web_servers/tasks/main.yml`
```
...
  notify: restart_apache
...

```

`roles/web_servers/handlers/main.yml`
```
- name: restart_apache
  service:
    name: "{{ apache_service }}"
    state: restarted
```

```
ansible-playbook site.roles.yml
```
