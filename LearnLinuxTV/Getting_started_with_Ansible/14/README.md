## Roles
* https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_14_-_Roles

```
roles=""
roles="${roles} base"
roles="${roles} db-servers"
roles="${roles} file_servers"
roles="${roles} web_servers"
roles="${roles} workstations"

for r in ${roles}; do
  mkdir -p roles/$r/tasks
  touch roles/$r/tasks/main.yml
done
```

```
roles/
├── base
│   └── tasks
│       └── main.yml
├── db_servers
│   └── tasks
│       └── main.yml
├── file_servers
│   └── tasks
│       └── main.yml
├── web_servers
│   └── tasks
│       └── main.yml
└── workstations
    └── tasks
        └── main.yml
```


```
ansible all -u root -m ping
ansible all -m ping
ansible all -v  -m ping
ansible all -vv -m ping
ansible all -vv -m ping
```

```
ansible-playbook site.roles.yml
```
