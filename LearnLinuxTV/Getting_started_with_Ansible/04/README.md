## Getting Started with Ansible 04 - Executing ad-hoc Commands


### Install ansible host
* https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

#### Debian based (Debian, Ubuntu, ...)

```
sudo apt update
sudo apt install ansible
```

#### RHEL based (RHEL, CentOS, Oracle Linux)

```
sudo dnf install ansible
```
or
```
sudo yum install ansible
```

#### macOS
```
python -m pip install --user ansible
```
#### Windows
* Windows is not supported for the control node
* http://blog.rolpdog.com/2020/03/why-no-ansible-controller-for-windows.html
  * If you want to run an Ansible controller on Windows anytime soon, use WSL, WSL2
  * https://docs.microsoft.com/en-us/windows/wsl/compare-versions#whats-new-in-wsl-2

### Test Ansible is working
* see [../my_lxd_homelab](../my_lxd_homelab/)



`inventory`
```
ub20-tut-ansible-01
ub18-tut-ansible-01
de10-tut-ansible-01
de09-tut-ansible-01
ce83-tut-ansible-01
ce79-tut-ansible-01
```

* ToDo: if python3 is not installed
```
[debian:vars]
ansible_python_interpreter=/usr/bin/python
```

```
ansible all -u root --key-file ~/.ssh/id_ansible_ed25519 -i inventory -m ping
```


`ansible.cfg`
```
[defaults]
INVENTORY = inventory
PRIVATE_KEY_FILE = ~/.ssh/id_ansible_ed25519
REMOTE_USER = root
```

```
ansible all -m ping
ansible all --list-hosts
ansible all -m gather_facts
ansible all -m gather_facts --limit ub20-tut-ansible-01
```

### Documentation
* https://docs.ansible.com/ansible/latest/reference_appendices/config.html
* https://docs.ansible.com/ansible/latest/installation_guide/intro_configuration.html
* https://docs.ansible.com/ansible/latest/cli/ansible-config.html#ansible-config

