## Managing Files

```
mkdir files
```

`files/default_site.html`
```
<html>
  <title>Web-site test</title>
  <body>
Ansible is awesome!
  </body>
</html>
```


```
   - name: copy default html
     tags: apache,apache2,httpd
     copy:
       src: default_site.html
       dest: /var/www/html/index.html
       owner: root
       group: root
       mode: 0644
```

```
ansible-playbook --tags apache2 site.yml
```

```
  - name: install terraform
    tags: terraform
    unarchive:
      src: https://releases.hashicorp.com/terraform/0.14.3/terraform_0.14.3_linux_amd64.zip
      dest: /usr/local/bin/
      remote_src: yes
      mode: 0755
      owner: root
      group: root
```

```
ansible-playbook --tags terraform site.yml
ansible-playbook --ask-become-pass file_management.yml
```
 
