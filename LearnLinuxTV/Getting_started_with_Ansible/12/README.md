## Managing Services

```
- name: start httpd (CentOS)
    tags: apache,centos,httpd
    when: ansible_distribution == "CentOS"
    service:
      name: httpd
      state: started
      enabled: yes

  - name: change e-mail for amdin
    tags: apache,apache2,ubuntu
    when: ansible_distribution == "CentOS"
    lineinfile:
      path: /etc/httpd/conf/httpd.conf
      regex: '^ServerAdmin'
      line: ServerAdmin foo@bar.com
    register: httpd

  - name: restart httpd (CentOS)
    tags: apache,apache2,ubuntu
    when: httpd.changed
    service:
      name: httpd
      state: restarted
```

```
ansible-playbook --tags httpd site.1.yml
ansible-playbook --tags httpd site.2.yml
ansible-playbook --tags httpd site.3.yml
```
