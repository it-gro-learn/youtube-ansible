## The "when" Conditional

`install_tools.yml`

```
- hosts: all
  become: true
  tasks:

  - name: update repo index
    when: ansible_distribution in ["Debian", "Ubuntu"]
    apt:
      update_cache: no

  - name: update repo index
    when: ansible_distribution in ["CentOS", "Red Hat Enterprise Linux"]
    yum:
      update_cache: no

  - name: install sudo
    when: ansible_distribution in ["Debian", "Ubuntu"]
    apt:
      name: sudo

  - name: install sudo
    when: ansible_distribution in ["CentOS", "Red Hat Enterprise Linux"]
    yum:
      name: sudo

  - name: install tools
    when: ansible_distribution in ["Debian", "Ubuntu"]
    apt:
      name: nano,vim,tmux,mc,htop
      state: latest

  - name: install tools
    when: ansible_distribution in ["CentOS", "Red Hat Enterprise Linux"]
    yum:
      name: nano,vim,tmux,mc
      state: latest
```


```
ansible-playbook --ask-become-pass install_tools.yml
```

```
ansible all      -m gather_facts --limit de10_tut_ansible_01 | grep distribution
```
