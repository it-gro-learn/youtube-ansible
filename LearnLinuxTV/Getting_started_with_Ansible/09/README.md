## Targeting Specific Nodes

```
# ansible all -u ansible -m ping
ansible all -m ping
```

```
ansible-playbook site.yml
ansible-playbook --ask-become-pass site.yml
```

```
ansible-playbook site.yml
```
