## Getting_Started_with_Ansible_05_-_Running_elevated_Commands
* https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_05_-_Running_elevated_Commands
* https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html

```
ansible all -u root -m apt -a update_cache=true
ansible all -u root -m apt -a update_cache=true --become --ask-become-pass
```


```
[ubuntu]
ub20_tut_ansible_01
ub18_tut_ansible_01

[debian]
de10_tut_ansible_01
de09_tut_ansible_01

[centos]
ce83_tut_ansible_01
ce79_tut_ansible_01

[debian:vars]
ansible_python_interpreter=/usr/bin/python3
```


```
ansible ubuntu,debian -m ping
```

```
ansible debian -m apt -a name=sudo
ansible centos -m yum -a name=sudo
```

```
ansible ubuntu,debian -m apt -a update_cache=true --become --ask-become-pass
ansible ubuntu,debian -m apt -a name=tmux --become --ask-become-pass
ansible ubuntu        -m apt -a "name=sudo state=latest" --become --ask-become-pass
ansible ubuntu,debian -m apt -a upgrade=dist --become --ask-become-pass

```
