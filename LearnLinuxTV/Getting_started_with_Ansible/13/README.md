## Managing Users & Bootstrapping

```
 - hosts: all
   become: true
   tasks:

   - name: create simone user
     tags: always
     user:
       name: simone
       groups: root

```

```
   - name: add sudoers file for simone
     tags: always
     copy:
       src: sudoer_simone
       dest: /etc/sudoers.d/simone
       owner: root
       group: root
       mode: 0440

```

`bootstrap.yml`
```
 ---
 
 - hosts: all
   become: true
   pre_tasks:
 
   - name: install updates (CentOS)
     tags: always
     dnf:
       update_only: yes
       update_cache: yes
     when: ansible_distribution == "CentOS"
 
   - name: install updates (Ubuntu)
     tags: always
     apt:
       upgrade: dist
       update_cache: yes
     when: ansible_distribution == "Ubuntu"
 
 - hosts: all
   become: true
   tasks:
 
   - name: create simone user
     user:
       name: simone
       groups: root
 
   - name: add ssh key for simone
     authorized_key:
       user: simone
       key: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAe7/ofWLNBq3+fRn3UmgAizdicLs9vcS4Oj8VSOD1S/ ansible"
      
   - name: add sudoers file for simone
     copy:
       src: sudoer_simone
       dest: /etc/sudoers.d/simone
       owner: root
       group: root
       mode: 0440
```

```
   - name: update repository index (CentOS)
     tags: always
     dnf:
       update_cache: yes
     changed_when: false
     when: ansible_distribution == "CentOS"
 
   - name: update repository index (Ubuntu)
     tags: always
     apt:
       update_cache: yes
     changed_when: false
     when: ansible_distribution == "Ubuntu"

```


```
ansible-playbook --tags httpd site.1.yml
ansible-playbook --tags httpd site.2.yml
ansible-playbook --tags httpd site.3.yml
```
