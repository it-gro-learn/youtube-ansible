# Getting started with Ansible
* https://www.learnlinux.tv/getting-started-with-ansible/

```
Episode Number 	Title
01              Introduction
02              SSH overview & Setup
03              Setting up the Git Repo
04              Executing ad-hoc Commands
05              Running elevated Commands
06              Writing our first Playbook
07              The "when" Conditional
08              Improving your Playbook
09              Targeting Specific Nodes
10              Tags
11              Managing Files
12              Managing Services
13              Managing Users
14              Roles
15              Host Variables
16              Templates
```

## YouTube Playlist
 * Playlist [Getting started with Ansible](https://www.youtube.com/playlist?list=PLT98CRl2KxKEUHie1m24-wkyHpEsa4Y70)
   * Part 1 (Getting started): https://learnlinux.link/ansible1
   * Part 2 (SSH Overview & Setup): https://learnlinux.link/ansible2
   * Part 3 (Setting up the Git Repository):  https://learnlinux.link/ansible3
   * Part 4 (Running Commands): https://learnlinux.link/ansible4
   * Part 5 (Running Elevated Commands): https://learnlinux.link/ansible5
   * Part 6 (Writing our first Playbook): https://learnlinux.link/ansible6
   * Part 7 (The "when" Conditional): https://learnlinux.link/ansible7
   * Part 8 (Improving your Playbook): https://learnlinux.link/ansible8
   * Part 9 (Targeting Specific Nodes): https://learnlinux.link/ansible9
   * Part 10 (Tags): https://learnlinux.link/ansible10
   * Part 11 (Managing Files): https://learnlinux.link/ansible11
   * Part 12 (Managing Services): https://learnlinux.link/ansible12
   * Part 13 (Adding Users & Bootstrapping): https://learnlinux.link/ansible13
   * Part 14 (Roles): https://learnlinux.link/ansible14
   * Part 15 (Host Variables): https://learnlinux.link/ansible15
   * Part 16 (Templates): https://learnlinux.link/ansible16


## wiki.learnlinux.tv
* https://wiki.learnlinux.tv/index.php/Episode_Notes
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_01_-_Introduction
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_02_-_SSH_overview_%26_Setup
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_03_-_Setting_up_the_Git_Repo
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_04_-_Executing_ad-hoc_Commands
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_05_-_Running_elevated_Commands
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_06_-_Writing_our_first_Playbook
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_07_-_The_%22when%22_Conditional
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_08_-_Improving_your_Playbook
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_09_-_Targeting_Specific_Nodes
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_10_-_Tags
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_11_-_Managing_Files
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_12_-_Managing_Services
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_13_-_Managing_Users
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_14_-_Roles
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_15_-_Host_Variables
  * https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_16_-_Templates
