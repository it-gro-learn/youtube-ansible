## Templates
* https://wiki.learnlinux.tv/index.php/Getting_Started_with_Ansible_16_-_Templates


`/etc/ssh/sshd_config`

```
mkdir roles/base/templates

# j2: jinja2 format
lxc file pull ub20-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_ubuntu_20.j2
lxc file pull ce83-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_centos_83.j2
lxc file pull de10-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_debian_10.j2

lxc file pull ub18-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_ubuntu_18.j2
lxc file pull ce79-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_centos_79.j2
lxc file pull de09-tut-ansible-01/etc/ssh/sshd_config roles/base/templates/sshd_config_debian_09.j2
```

`sshd_config*.j2`
```
AllowUsers {{ ssh_users }}
```

`host_vars/ub18-tut-ansible-01.yml`
```
ssh_users: "root ansible ubuntu grossnik"
ssh_template_file: sshd_config_ubuntu_18.j2
```


`roles/base/tasks/main.yml`
```
- name: openssh | generate ssh_config file from template
  tags: ssh
  template:
    src: "{{ ssh_template_file }}"
    dest: /etc/ssh/sshd_config
    owner: root
    group: root
    mode: 0644
  notify: restart_sshd
```


```
mkdir roles/base/handlers
```

`roles/base/handlers/main.yml`
```
- name: restart_sshd
  service:
    name: sshd
    state: restarted
```

```
ansible-playbook site.roles.yml
```

host_vars/ub20-tut-ansible-01.yml
```
PasswordAuthentication: "no"
```

```
PasswordAuthentication {{ PasswordAuthentication }}
```


```
lxc list

lxc file edit ce79-tut-ansible-01/etc/ssh/sshd_config
lxc file edit ce83-tut-ansible-01/etc/ssh/sshd_config

lxc file edit de09-tut-ansible-01/etc/ssh/sshd_config
lxc file edit de10-tut-ansible-01/etc/ssh/sshd_config


lxc file edit ub18-tut-ansible-01/etc/ssh/sshd_config
lxc file edit ub20-tut-ansible-01/etc/ssh/sshd_config
```
