# Jeff Geerling

## Ansible
* [flexible_maintainable_scalable](flexible_maintainable_scalable)

# ToDo
* Ansible Playlist (15 Episodes)
  * https://www.youtube.com/playlist?list=PL2_OBreMn7FplshFCWYlaN2uS8et9RjNG
    * Ansible 101 - Episode 1 - Introduction to Ansible
    * Ansible 101 - Episode 1 - Introduction to Ansible
    * Ansible 101 - Episode 2 - Ad-hoc tasks and Inventory
    * Ansible 101 - Episode 3 - Introduction to Playbooks
    * Ansible 101 - Episode 4 - Your first real-world playbook
    * Ansible 101 - Episode 5 - Playbook handlers, environment vars, and variables
    * Ansible 101 - Episode 6 - Ansible Vault and Roles
    * Ansible 101 - Episode 6 - Ansible Vault and Roles
    * Ansible 101 - Episode 7 - Molecule Testing and Linting and Ansible Galaxy
    * Ansible 101 - Episode 7 - Molecule Testing and Linting and Ansible Galaxy
    * Ansible 101 - Episode 8 - Playbook testing with Molecule and GitHub Actions CI
    * Ansible 101 - Episode 9 - First 5 min server security with Ansible
    * Ansible 101 - Episode 10 - Ansible Tower and AWX
    * Ansible 101 - Episode 11 - Dynamic Inventory and Smart Inventories
    * Ansible 101 - Episode 12 - Real-world Ansible Playbooks
    * Ansible 101 - Episode 13 - Ansible Collections and a Test Plugin
    * Ansible 101 - Episode 14 - Ansible and Windows
    * Ansible 101 - Episode 15 - Final LIVE Q&A
