# TechWorld with Nana
* https://www.techworld-with-nana.com
* https://dev.to/techworld_with_nana
* https://www.instagram.com/techworld_with_nana
* https://gitlab.com/nanuchi
* https://www.youtube.com/c/TechWorldwithNana
* Playlist DevOps Tools: https://www.youtube.com/playlist?list=PLy7NrYWoggjxKDRWLqkd4Kbt84XEerHhB

## Ansible
* https://www.youtube.com/c/TechWorldwithNana/search?query=ansible
* [What_is_Ansible](What_is_Ansible)
