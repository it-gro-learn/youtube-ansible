# What is Ansible | Ansible Playbook explained | Ansible Tutorial for Beginners
* https://www.youtube.com/watch?v=1id6ERvfozo
* [screenshots](screenshots)

```
00:00 - Start
00:26 - What is Ansible?
00:45 - Why use Ansible?
04:13 - Ansible is agentless
05:20 - Ansible Modules explained
07:00 - YAML Syntax
07:48 - Ansible Playbook explained
11:50 - Ansible Inventory
12:47 - Ansible for Docker
14:40 - Ansible Tower
15:05 - Ansible vs Puppet and Chef
```
